package soh.bykowsky.model;

public class MysteriousVariable {
    private int mysteriousVariableID;
    private int getMysteriousVariableValue;

    public MysteriousVariable() {
    }

    public MysteriousVariable(int mysteriousVariableID, int getMysteriousVariableValue) {
        this.mysteriousVariableID = mysteriousVariableID;
        this.getMysteriousVariableValue = getMysteriousVariableValue;
    }

    @Override
    public String toString() {
        return "MysteriousVariable{" +
                "mysteriousVariableID=" + mysteriousVariableID +
                ", getMysteriousVariableValue=" + getMysteriousVariableValue +
                '}';
    }

    // getters and setters
    public int getMysteriousVariableID() {
        return mysteriousVariableID;
    }

    public void setMysteriousVariableID(int mysteriousVariableID) {
        this.mysteriousVariableID = mysteriousVariableID;
    }

    public int getGetMysteriousVariableValue() {
        return getMysteriousVariableValue;
    }

    public void setGetMysteriousVariableValue(int getMysteriousVariableValue) {
        this.getMysteriousVariableValue = getMysteriousVariableValue;
    }
}
