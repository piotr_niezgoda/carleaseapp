package soh.bykowsky.model;

public class User {
    private int userId;
    private String login;
    private String password;
    private String userType;
    private String name;
    private String surname;

    public User() {
    }

    public User(String login, String userType) {
        this.login = login;
        this.userType = userType;
    }

    public User(String login, String userType, String name, String surname) {
        this.login = login;
        this.userType = userType;
        this.name = name;
        this.surname = surname;
    }

    public User(String login, String password, String userType, String name, String surname) {
        this.login = login;
        this.password = password;
        this.userType = userType;
        this.name = name;
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", login='" + login + '\'' +
                ", userType='" + userType + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    // getters and setters;
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
