package soh.bykowsky.service;

import org.springframework.stereotype.Component;
import soh.bykowsky.DBhandler;
import soh.bykowsky.model.MysteriousVariable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class MysteriousVariableServiceImpl implements MysteriousVariableService {
    @Override
    public MysteriousVariable getVariable(int variableID) {
        try {
            String SQL = "SELECT * FROM TmysteriousVariables WHERE mysteriousVariableID = ?";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setInt(1, variableID);
            ResultSet results = preparedStatement.executeQuery();

            MysteriousVariable mysteriousVariableFromDB = new MysteriousVariable();
            while (results.next()) {
                mysteriousVariableFromDB.setMysteriousVariableID(variableID);
                mysteriousVariableFromDB.setGetMysteriousVariableValue(results.getInt("mysteriousVariableValue"));
            }

            return mysteriousVariableFromDB;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean updateVariable(int variableID, int variableValue) {
        try {
            String SQL = "UPDATE TmysteriousVariables SET mysteriousVariableValue = ? WHERE mysteriousVariableID = ?";
            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setInt(1, variableValue);
            preparedStatement.setInt(2, variableID);
            preparedStatement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean addVariable(MysteriousVariable mysteriousVariable) {
        //TODO to implement in the near future. Right now not needed
        return false;
    }

    @Override
    public boolean deleteVariable(int variableID) {
        //TODO to implement in the near future. Right now not needed
        return false;
    }
}
