package soh.bykowsky.service;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

@Component
public class PriceCalculatorImpl implements PriceCalculator {

    @Override
    public BigDecimal calculateTotalPrice(BigDecimal basePrice, BigDecimal amortizationPrice) {
        return basePrice.add(amortizationPrice).round(new MathContext(2, RoundingMode.HALF_EVEN));
    }

    @Override
    public BigDecimal calculateAmortizationPrice(int mysteriousVariable, String type){
        BigDecimal typeModifier = new BigDecimal(1);

        if (type.equals("osobowy"))
            typeModifier = new BigDecimal(0.5);

        if (type.equals("ciężarowy"))
            typeModifier = new BigDecimal(0.7);

        if (type.equals("maszyna"))
            typeModifier = new BigDecimal(0.99);

        return typeModifier.multiply(BigDecimal.valueOf(mysteriousVariable));
    }
}
