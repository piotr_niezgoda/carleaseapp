package soh.bykowsky.service;

import java.math.BigDecimal;

public interface PriceCalculator {
    BigDecimal calculateTotalPrice(BigDecimal basePrice, BigDecimal amortizationPrice);
    BigDecimal calculateAmortizationPrice(int mysteriousVariable, String type);
}
