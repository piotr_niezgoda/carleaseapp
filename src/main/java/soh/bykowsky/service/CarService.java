package soh.bykowsky.service;

import soh.bykowsky.model.Car;

import java.util.List;

public interface CarService {
    Car getCar(int id);
    Car getCar(String brand, String name);

    List<Car> getCarsAll();
    List<Car> getCarsByBrand(String brand);
    List<Car> getCarsByType(String type);
    List<Car> getCarsBySubType(String subType);

    boolean addCar(Car car);
    boolean deleteCar(int id);
    public boolean updateCar(int id, Car car);

    public List<String> getBrands();
}
