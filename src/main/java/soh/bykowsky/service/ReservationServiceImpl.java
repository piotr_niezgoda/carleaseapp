package soh.bykowsky.service;

import org.springframework.stereotype.Component;
import soh.bykowsky.DBhandler;
import soh.bykowsky.model.Reservation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ReservationServiceImpl implements ReservationService {
    @Override
    public Reservation getReservation(int reservationID) {
        try {
            String SQL = "SELECT * FROM Treservations WHERE reservationID = ?";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setInt(1, reservationID);
            ResultSet results = preparedStatement.executeQuery();

            Reservation reservationFromDB = new Reservation();
            while (results.next()) {
                reservationFromDB.setReservationID(reservationID);
                reservationFromDB.setUserID(results.getInt("userID"));
                reservationFromDB.setCarID(results.getInt("carID"));
                reservationFromDB.setDate(results.getDate("date").toLocalDate());
                reservationFromDB.setReservationCost(results.getDouble("reservationCost"));
            }

            return reservationFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean addReservation(Reservation reservation) {
        try {
            String SQL = "INSERT INTO Treservations (userID, carID, date, reservationCost) VALUES (?, ?, ?, ?)";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setInt(1, reservation.getUserID());
            preparedStatement.setInt(2, reservation.getCarID());
            preparedStatement.setDate(3, java.sql.Date.valueOf(reservation.getDate()));
            preparedStatement.setDouble(4, reservation.getReservationCost());

            preparedStatement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteReservation(int reservationID) {
        try {
            String SQL = "DELETE FROM Treservations WHERE reservationID = ?";
            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setInt(1, reservationID);
            preparedStatement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
